﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Interfaces;
using Common.Models;
using Dal.Core;

namespace Dal
{
    public class SqlOrderRepository : BaseRepository, IOrderRepository
    {

        public List<OrderModel> GetOrders(params int[] ids)
        {
            var all = ids == null;
            ids = ids ?? (new int[0]);
            return Context.Order.Where(x => all || ids.Contains(x.id)).Select(x => new OrderModel
            {
                Id = x.id,
                OrderNum = x.OrderNumber
            }).ToList();

        }

        public List<int> GetOrderIds(int skip, int take)
        {
            return Context.Order
                .OrderBy(x => x.id)
                .Skip(skip)
                .Take(take)
                .Select(x => x.id)
                .ToList();
        }
    }
}
