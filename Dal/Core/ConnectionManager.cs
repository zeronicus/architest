using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Common;
using Common.Interfaces;
using Ninject;

namespace Dal.Core
{
    //��� �������� ���������� �����������. ������ ��������� - ��� ������-������, ������ - ��� ������������
    public class ConnectionManager : IConnectionManager, IContextRepository
    {
        //�������������������-��. ���� - �� �����, �������� - ������ �������� ������� ����������
        private readonly ConcurrentDictionary<int, List<EdmontContext>> _contexts;
        private IKernel _kernel;
        private int ThreadId { get { return System.Threading.Thread.CurrentThread.ManagedThreadId; } }
        public ConnectionManager(IKernel kernel)
        {
            _kernel = kernel;
            Debug.WriteLine("cm init"+ThreadId);
            //
            _contexts = new ConcurrentDictionary<int, List<EdmontContext>>();
        }
        //��������� ����� ���������
        public IDisposable WithOneConnection()
        {
            Debug.WriteLine("one connect "+ThreadId);
            //������� ��������� � ������ � � ������ ��������� ��� ����� �����
            var context = new EdmontContext();
            List<EdmontContext> currThrList;
            if (!_contexts.TryGetValue(ThreadId, out currThrList))
            {
                currThrList = new List<EdmontContext>();
                _contexts[ThreadId]=currThrList;
            }
            currThrList.Add(context);
            return new SimpleDisposable(() =>
            {
                //��� �������� ��������� �������� � ������� ��� �� ������ ���������
                Debug.WriteLine("end one connect " + ThreadId);
                context.Dispose();
                currThrList.Remove(context);
                if (currThrList.Count == 0)
                    _contexts.TryRemove(ThreadId, out currThrList);
            });
        }

        public ITransaction BeginTransaction(IsolationLevel level = IsolationLevel.Serializable)
        {
            return new Transaction(GetThreadContext(), level);
        }

        //���������� ���������� �������� ��� �����(���� �� ����)
        private EdmontContext GetConcretContext()
        {
            List<EdmontContext> concContex;
            if (_contexts.TryGetValue(ThreadId, out concContex))
            {
                if (concContex.Count > 0)
                    return concContex.LastOrDefault();
            }
            return null;
        }
        //���������� �������� ��� �����, ������� ������ ���� ������������.
        //���� ��� �����������, �� ����� �� ioc
        public EdmontContext GetThreadContext()
        {
            return GetConcretContext() ?? _kernel.Get<EdmontContext>();
        }


        public bool HasConcretContext
        {
            get { return GetConcretContext() != null; }
        }
    }
}