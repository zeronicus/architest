﻿using Common.Interfaces;
using Common.Models;
using Ninject;
using Ninject.Web.Common;

namespace Dal.Core
{
    public static class DalDependencyRegistator
    {
        public static void Register(IKernel kernel, ApplicationType type)
        {
            switch (type)
            {
                case ApplicationType.Web: //Для веба время жизни контекста по дефолту - реквест
                    kernel.Bind<EdmontContext>().ToSelf().InRequestScope();
                    break;
                case ApplicationType.Win://Для вина - один вызов метода. По умолчанию он не диспоузится, диспоуз делает интерсептор(см. BaseRepository)
                    kernel.Bind<EdmontContext>().ToSelf().InTransientScope();
                    break;
            }
            //Это ядро управления контекстами, оно реализует 2 интерфейса. Синглтон, thread-safe
            kernel.Bind<ConnectionManager>().ToSelf().InSingletonScope();
            kernel.Bind<IConnectionManager>().ToMethod(ctx => kernel.Get<ConnectionManager>());
            kernel.Bind<IContextRepository>().ToMethod(ctx => kernel.Get<ConnectionManager>());
        }
    }
}
