﻿using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using Common.Interfaces;

namespace Dal.Core
{
    internal class Transaction : ITransaction
    {
        private bool _commited;
        private readonly DbContextTransaction _dbContextTransaction;

        public Transaction(EdmontContext context, IsolationLevel level)
        {
            _dbContextTransaction = context.Database.BeginTransaction(level);
            _commited = false;
        }

        public void Dispose()
        {
            try
            {
                if (!_commited)
                    RollBack();
            }
            finally
            {
                _dbContextTransaction.Dispose();
            }
        }

        public void Commit()
        {
            Debug.WriteLine("commit");
            _commited = true;
            _dbContextTransaction.Commit();
        }

        public void RollBack()
        {
            Debug.WriteLine("rollback");
            _dbContextTransaction.Rollback();
        }
    }
}