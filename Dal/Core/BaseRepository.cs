﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Ninject;

namespace Dal.Core
{
    //Базовый класс для всех репозиториев
    //В аттрибуте интерсептор, который подставляет нужный контекст для вызова метода
    [Context]
    public class BaseRepository
    {
        //К экземпляру могут обращаться из нескольких потоков
        private readonly ConcurrentDictionary<int, EdmontContext> _contexts = new ConcurrentDictionary<int, EdmontContext>();
        //Возвращает и задает контекст для заданного треда
        internal EdmontContext Context
        {
            get { return _contexts[CreateThreadNum]; }
            set
            {
                EdmontContext tmp;
                if (value == null) //очищаем dictionary
                    _contexts.TryRemove(CreateThreadNum, out tmp);
                else
                {
                    _contexts[CreateThreadNum] = value;
                }
                
            }
        }
        [Inject]
        public IContextRepository ContextRepository { internal get; set; }
        private int CreateThreadNum
        {
            get { return System.Threading.Thread.CurrentThread.ManagedThreadId; }
        }
    }
}
