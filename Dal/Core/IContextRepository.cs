namespace Dal.Core
{
    public interface IContextRepository
    {
        EdmontContext GetThreadContext();
        bool HasConcretContext { get; }
    }
}