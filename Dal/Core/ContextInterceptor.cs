using System;
using System.ComponentModel;
using System.Diagnostics;
using Common.Models;
using Ninject;
using Ninject.Extensions.Interception;

namespace Dal.Core
{
    public class ContextInterceptor:SimpleInterceptor
    {
        [Inject]
        public ApplicationType ApplicationType { get; set; }
        protected override void BeforeInvoke(IInvocation invocation)
        {
            var repository = (invocation.Request.Target as BaseRepository);
            if (repository==null)
                throw new ArgumentException("This attribute can use only with BaseRepository");
            //����� ������� ������ ����� ���������� �������� ��� ��������� �����, � ������ ��� � �����������
            var context = repository.ContextRepository.GetThreadContext();
            Debug.WriteLine("Start Call By " + context.Num + " " + System.Threading.Thread.CurrentThread.ManagedThreadId);
            (invocation.Request.Target as BaseRepository).Context = context;
            base.BeforeInvoke(invocation);
        }

        protected override void AfterInvoke(IInvocation invocation)
        {
            var repository = (invocation.Request.Target as BaseRepository);
            if (repository == null)
                throw new ArgumentException("This attribute can use only with BaseRepository");
            Debug.WriteLine("Stop Call By " + repository.Context.Num + " " + System.Threading.Thread.CurrentThread.ManagedThreadId);
            //��������� - ���� � ��� ���-������, � ��������� ������ �� �������(�.�. �� ���� ����� ������ - ���� ��������), �� ���� �������� ���������
            if (!repository.ContextRepository.HasConcretContext && ApplicationType == ApplicationType.Win)
            {
                Debug.WriteLine("Dispose in interceptor " + repository.Context.Num + " " + System.Threading.Thread.CurrentThread.ManagedThreadId);
                repository.Context.Dispose(); 
            }
            repository.Context = null;
        }
    }
}