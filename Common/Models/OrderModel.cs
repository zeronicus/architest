﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string OrderNum { get; set; }
        public List<OrderLineModel> OrderLines { get; set; }
    }

    public class OrderLineModel
    {
        public int OrderLineId { get; set; }
        public string OrderNum { get; set; }
        public string PartNumber { get; set; }
        public int Quantity { get; set; }
    }
    public enum ApplicationType
    {
        Web = 1,
        Win = 2,
    }
}
