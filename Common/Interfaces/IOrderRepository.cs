﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Models;

namespace Common.Interfaces
{
    public interface IOrderRepository
    {
        List<OrderModel> GetOrders(params int[] ids);
        List<int> GetOrderIds(int skip, int take);
    }
}
