﻿using System;
using System.Data;

namespace Common.Interfaces
{
    public interface IConnectionManager
    {
        IDisposable WithOneConnection();
        ITransaction BeginTransaction(IsolationLevel level = IsolationLevel.Serializable);
    }

    public interface ITransaction : IDisposable
    {
        void Commit();
        void RollBack();
    }
}