﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Common.Interfaces;
using Dal;
using Ninject;

namespace ArchiTest.Controllers
{
    public class HomeController : Controller
    {
        //NInject будет подставлять при создании инстанса(т.е. на реквест)
        [Inject]
        public IOrderRepository OrderRepository { get; set; }
        [Inject]
        public IConnectionManager ConnectionManager { get; set; }

        /// <summary>
        /// Создается один контекст, с ним выполняются оба метода, после реквеста контекст диспоузится
        /// </summary>
        /// <returns></returns>
        public ActionResult Do1()
        {
            var ids = OrderRepository.GetOrderIds(0, 10);
            var orders = OrderRepository.GetOrders(ids.ToArray());
            return View("Index", orders);
        }

        public ActionResult Do2()
        {
            //создается контекст 1, в нем выполняются 2 метода
            var ids = OrderRepository.GetOrderIds(0, 10);
            var orders = OrderRepository.GetOrders(ids.ToArray());
            Debug.WriteLine("Create context 2");
            using (ConnectionManager.WithOneConnection()) //создается контекст 2, в нем выполняются 2 метода, он диспоузится
            {
                ids = OrderRepository.GetOrderIds(0, 10);
                orders = OrderRepository.GetOrders(ids.ToArray());
            }
            //выполняются в первом контексте, после он диспоузится
            ids = OrderRepository.GetOrderIds(0, 10);
            orders = OrderRepository.GetOrders(ids.ToArray());
            return View("Index", orders);
        }
        /// <summary>
        /// Transaction with commit
        /// </summary>
        /// <returns></returns>
        public ActionResult Do3()
        {
            //создется один контекст, в нем выполняется транзакция, коммитится, контекст диспоузится
            using (var transaction = ConnectionManager.BeginTransaction()) 
            {
                var ids = OrderRepository.GetOrderIds(0, 10);
                var orders = OrderRepository.GetOrders(ids.ToArray());
                transaction.Commit();
                return View("Index", orders);
            }
        }
        /// <summary>
        /// Transaction with commit in custom connection
        /// </summary>
        /// <returns></returns>
        public ActionResult Do4()
        {
            //выполняется в контексте 1
            OrderRepository.GetOrderIds(0, 10);
            //создается контекст 2
            using (ConnectionManager.WithOneConnection())
            {
                //транзакция и методы выполняются в контексте 2, он диспоузится
                using (var transaction = ConnectionManager.BeginTransaction())
                {
                    var ids = OrderRepository.GetOrderIds(0, 10);
                    var orders = OrderRepository.GetOrders(ids.ToArray());
                    transaction.Commit();
                    return View("Index", orders);
                }
            }
            //диспоуз 1 контекста
        }
        /// <summary>
        /// Transaction with rollback
        /// </summary>
        /// <returns></returns>
        public ActionResult Do5()
        {
            //а тут ролбэк
            using (ConnectionManager.BeginTransaction())
            {
                var ids = OrderRepository.GetOrderIds(0, 10);
                var orders = OrderRepository.GetOrders(ids.ToArray());
                return View("Index", orders);
            }
        }
        public ActionResult Do6()
        {
            //большой тест
            var t1 = new Thread(() =>
            {
                //Так делать не надо. Если руками делаешь новый тред, то надо и коннекциями самому управлять
                //Метод выполнится - под тред создастся контекст, но он не будет задиспоузен
                OrderRepository.GetOrders();
                //а вот так делать надо. Создастся контекст, в нем и выполнится вызов
                //внимание: и репозитории, и ConnectionManager Thread-safe
                using (ConnectionManager.WithOneConnection())
                {
                    OrderRepository.GetOrders();
                }
            });
            var t2 = new Thread(() =>
            {
                //вот так и надо делать
                using (ConnectionManager.WithOneConnection())
                {
                    OrderRepository.GetOrders();
                }
            });
            t1.Start();
            t2.Start();
            //а эти 2 метода выполнятся в контексте основного потока(будет задиспоузен после конца реквеста)
            var ids = OrderRepository.GetOrderIds(0, 10);
            var orders = OrderRepository.GetOrders(ids.ToArray());
            return View("Index");
        }
    }
}