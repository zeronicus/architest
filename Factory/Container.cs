﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Interfaces;
using Common.Models;
using Dal;
using Dal.Core;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;

namespace Factory
{
    
    public static class Container
    {
        internal static IBindingNamedWithOrOnSyntax<TTo> BindForType<TFrom, TTo>(ApplicationType type) where TTo:TFrom
        {
            switch (type)
            {
                case ApplicationType.Web:
                    return Kernel.Bind<TFrom>().To<TTo>().InRequestScope();
                case ApplicationType.Win:
                    return Kernel.Bind<TFrom>().To<TTo>().InThreadScope();
            }
            return null;
        }
        public static IKernel Kernel { get; set; }

        public static T GetService<T>()
        {
            return (T)Kernel.GetService(typeof(T));
        }

        private static void RegisterAll(ApplicationType type)
        {
            //Вот тут все основные модули регистрируются
            BindForType<IOrderRepository,SqlOrderRepository>(type);
        }
        public static void RegisterWebAppServices()
        {
            //Указываем какой у нас тип приложения - веб или вин
            Kernel.Bind<ApplicationType>().ToConstant(ApplicationType.Web).InSingletonScope();
            //Регистрируем все модули(репозитории, сервисы, бл)
            RegisterAll(ApplicationType.Web);
            //Регистрация ядра DAL - в отдельном методе, чтобы контекст наружу не вытаскивать
            DalDependencyRegistator.Register(Kernel, ApplicationType.Web);
        }
    }
}
